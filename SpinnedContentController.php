<?php

/*
 * This file is part of the Emprunte Mon Toutou - API package.
 *
 * (c) Breith Barbot <b.breith@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\v2\Admin;

use App\Utils\Generate;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 *
 * @IsGranted("ROLE_ADMIN")
 */
class SpinnedContentController extends AbstractController
{
    /**
     * Generate spinned content.
     *
     * @Route(path="/spinned-content", methods={"POST"})
     */
    public function spinningGeneration(Request $request, Generate $generate): JsonResponse
    {
        // Variables
        $text = $request->request->get('text');
        $count = $request->request->get('count');
        $array = [];

        for ($i = 0; $i < $count; ++$i) {
            $array[$i] = $generate->spinning($text);
        }

        return $this->json($array);
    }

    /**
     * Generate a spinned content.
     */
    private function spinning(string $text): string
    {
        preg_match_all('#{([^{}]*)}#msi', $text, $out);
        $toFind = [];
        $toReplace = [];
        foreach ($out[0] as $id => $match) {
            $choices = explode('|', $out[1][$id]);
            $toFind[] = $match;
            $toReplace[] = trim($choices[random_int(0, \count($choices) - 1)]);
        }
        $textSpinned = str_replace($toFind, $toReplace, $text);
        if (mb_strpos($textSpinned, '{') || mb_strpos($textSpinned, '}')) {
            return $this->spinning($textSpinned);
        }

        return $textSpinned;
    }
}